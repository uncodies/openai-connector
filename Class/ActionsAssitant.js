const OrderLine = require("./OrderLine");
const Product = require("./Product");

class ActiosAssistent{

    static parseAndGetPossiblesProducts(possible_matchs){
        return possible_matchs.split(',').map(item => item.trim());
    }
    static async getPossiblesProducts (product_skus){
        const promisesProducts = product_skus.map(
            (sku) => {
                return Product.findProductBySku(sku);
            }
        );
        const products = await Promise.all(promisesProducts);
        return products.reduce((acc, item) => { return acc.concat(item?.products || []) }, []);
    }
    static async create_possible_products(order_product_id, products, amount){
        
        
        const promises = products.map(
            (product) => OrderLine.addPossibleProduct(order_product_id, product._id, amount)
        );
        const possibleProducts = await Promise.all(promises);
        return possibleProducts;
    }
    static async create_order_line(user_id, tool, data, thread_id){
        try{
            const lastOrderResponse = await OrderLine.getLastOrderByUser(user_id);
            if(!lastOrderResponse.order.length)
                throw new Error("No order found");
            const productsBySku = await Product.findProductBySku(data.sku);
            const { products } = productsBySku;
            if(!products.length)
                throw new Error("No product found");
            
            const possible_matchs = this.parseAndGetPossiblesProducts(data.possible_matches);
            
            const messages = await global.openai.beta.threads.messages.list(thread_id);
            const reduceMessages = messages.data.reduce((tmp, item)=>{ return tmp.concat(item.content); }, []).filter(item=>item.type === 'text');
            const matchName =  reduceMessages.find(item=>item.text.value.toLowerCase().includes(products[0].name.toLowerCase()));
            const isConfirmed = ( typeof matchName === 'object' || data.possible_matches.length === 0) 
           
            const {  insert_order_product_one } = await OrderLine.addProductToOrder(lastOrderResponse.order[0]._id, parseInt(data.quantity), products[0]._id, isConfirmed);
            const orderProductId = insert_order_product_one._id;
            if(isConfirmed)
                return { 
                    tool_call_id: tool.id,
                    output: "success: true"      
                };
            
             
            possible_matchs.push(data.sku)
            const possibleProducts = await this.getPossiblesProducts(possible_matchs);
            await this.create_possible_products(orderProductId, possibleProducts, data.quantity);

            return { 
                tool_call_id: tool.id,
                output: "success: true"      
            };
        }catch(e){
            console.log(data, e);
            return { 
                tool_call_id: tool.id,
                output: `success: false`,
                
            };
        }
        
    }

    static async remove_order_line(user_id, tool, data){
        try{
            const lastOrderResponse = await OrderLine.getLastOrderByUser(user_id);
            if(!lastOrderResponse.order.length)
                throw new Error("No order found");
            const productsBySku = await Product.findProductBySku(data.sku);
            const { products } = productsBySku;
            if(!products.length)
                throw new Error("No product found");
            
            await OrderLine.removeOrderProduct(lastOrderResponse.order[0]._id, products[0]._id);
            return { 
                tool_call_id: tool.id,
                output: "success: true",
                
            };
        }catch(e){
            console.log(data, e);
            return { 
                tool_call_id: tool.id,
                output: `success: false`,
                
            };
        }
    }

    static async update_qty(user_id, tool, data){
        try{
            const lastOrderResponse = await OrderLine.getLastOrderByUser(user_id);
            if(!lastOrderResponse.order.length)
                throw new Error("No order found");
            const productsBySku = await Product.findProductBySku(data.sku);
            const { products } = productsBySku;
            if(!products.length)
                throw new Error("No product found");
            
            await OrderLine.updateOrderProduct(lastOrderResponse.order[0]._id, products[0]._id,  parseInt(data.quantity));
            return { 
                tool_call_id: tool.id,
                output: "success: true",
                
            };
        }catch(e){
            console.log(data, e);
            return { 
                tool_call_id: tool.id,
                output: `success: false`,
                
            };
        }
    }

    static async create_special_order(user_id, tool, data){
        try{
            const lastOrderResponse = await OrderLine.getLastOrderByUser(user_id);
            if(!lastOrderResponse.order.length)
                throw new Error("No order found");
            
            await OrderLine.addSpecialProduct(lastOrderResponse.order[0]._id, data.name, parseInt(data.quantity));
            return { 
                tool_call_id: tool.id,
                output: "success: true",
                
            };
        }catch(e){
            console.log(data, e);
            return { 
                tool_call_id: tool.id,
                output: `success: false`,
                
            };
        }
    }

    static async handleRequiresAction(run, thread_id, user_id){

        // Check if there are tools that require outputs
        if (!(
          run.required_action &&
          run.required_action.submit_tool_outputs &&
          run.required_action.submit_tool_outputs.tool_calls
        )) 
            return false;
        // Loop through each tool in the required action section
        const promises = run.required_action.submit_tool_outputs.tool_calls.filter(item=>(item.type === 'function')).map(
            (tool) => {
                switch (tool.function.name) {
                    case "create_order_line":
                        return this.create_order_line(user_id, tool, JSON.parse(tool.function.arguments), run.thread_id);
                    case "remove_order_line":
                        return this.remove_order_line(user_id, tool, JSON.parse(tool.function.arguments));
                    case "update_qty":
                        return this.update_qty(user_id, tool, JSON.parse(tool.function.arguments));
                    case "create_special_order":
                        return this.create_special_order(user_id, tool, JSON.parse(tool.function.arguments));
                    default:
                        return Promise.resolve({ 
                            tool_call_id: tool.id,
                            output: "success: false",
                            
                        });
                }
            }
        );
        const toolOutputs = await Promise.all(promises);
        // Submit all tool outputs at once after collecting them in a list
        if (toolOutputs.length > 0) {
            await openai.beta.threads.runs.submitToolOutputs(
                thread_id,
                run.id,
                { tool_outputs: toolOutputs }
            )   ;
            console.log("Tool outputs submitted successfully.");
        } else {
        console.log("No tool outputs to submit.");
        }
    
        // Check status after submitting tool outputs
        return true;
          
        
      };
}
module.exports = ActiosAssistent;
