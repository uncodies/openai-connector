const { gql } = require("graphql-request");
const { QueryFactory }  = require("../Utils/QueryFactory");

const getLastOrder = gql`
  query LastOrder($customer_id: Int!) {
    order(where: {customer_id: {_eq: $customer_id}}, limit: 1, order_by: {_id:desc}) {
      _id
    }
  }
`
const addProductToOrder = gql`
  mutation InsertProductOnOrder($order_id: Int!, $amount: Int!, $product_id: Int!, $is_confirmed: Boolean) {
    insert_order_product_one(object: {order_id: $order_id, product_id: $product_id, amount: $amount, is_confirmed: $is_confirmed }) {
      _id
    }
  }
`
const updateProductToOrder = gql`
  mutation UpdateProductToOrder($order_id: Int!, $amount: Int!, $product_id: Int!) {
    update_order_product(where: {order_id: {_eq: $order_id}, product_id: {_eq: $product_id}}, _set: {amount: $amount}) {
      affected_rows
    }
  }
`

class OrderLine {
  
    static getLastOrderByUser(userId) {
        return QueryFactory(getLastOrder, { customer_id: userId });
    }
    static addProductToOrder(order_id, amount, product_id, is_confirmed){
        return QueryFactory(addProductToOrder, { order_id, amount, product_id, is_confirmed });
    }
    static updateProductToOrder = (order_id, amount, product_id) => {
        return QueryFactory(updateProductToOrder, { order_id, amount, product_id });
    };

    static removeOrderProduct = (order_id, product_id) => {
        return QueryFactory(gql`
            mutation RemoveOrderProduct($order_id: Int!, $product_id: Int!) {
                delete_order_product(where: {order_id: {_eq: $order_id}, product_id: {_eq: $product_id}}) {
                    affected_rows
                }
            }
        `, { order_id, product_id });
    };

    static updateOrderProduct = (order_id, product_id, amount) => {
        return QueryFactory(gql`
            mutation UpdateOrderProduct($order_id: Int!, $product_id: Int!, $amount: Int!) {
                update_order_product(where: {order_id: {_eq: $order_id}, product_id: {_eq: $product_id}}, _set: {amount: $amount}) {
                    affected_rows
                }
            }
        `, { order_id, product_id, amount });
    }

    static  addSpecialProduct = (order_id, name, amount)=>{
        return QueryFactory(gql`
        mutation AddSpecialProduct($order_id: Int!, $name: String!, $amount: Int!) {
            insert_special_product_one(object: {order_id: $order_id, name: $name, amount: $amount}) {
                _id
            }
        }
        `, { order_id, name, amount });
    }

    static addPossibleProduct(order_product_id, product_id, amount){
        return QueryFactory(gql`
        mutation AddPossibleProduct($order_product_id: Int!, $product_id: Int!, $amount: Int!) {
            insert_possible_product_one(object: {order_product_id: $order_product_id, product_id: $product_id, amount: $amount}) {
                _id
            }
        }
        `, { order_product_id, product_id, amount });

    }
}

module.exports = OrderLine;