const { gql } = require("graphql-request");
const {QueryFactory}  = require("../Utils/QueryFactory");

const findProductBySku = gql`
    query findProducts($sku: String!) {
        products(where:{ sku: { _eq: $sku }}){
            _id
            name
        }
    }
`

const findByNameOrSku = gql`
query findProductsNameOrSku($criteria: String!) {
    products(where:{ _or: [ { sku: { _ilike: $criteria } }, { name: { _ilike: $criteria } } ] }, , order_by: { name: asc } ){
        _id
        name
        description
        sku
        avatar
        unity_price
        unity {
          name
        }
    }
}
`

class Product {
  
    static findProductBySku(sku) {
        return QueryFactory(findProductBySku, { sku });
    }

    static findProductByNameOrSku(criteria){
        return QueryFactory(findByNameOrSku, { criteria }); 
    }
    
}

module.exports = Product;