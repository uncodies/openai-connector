const ActiosAssistent = require("../Class/ActionsAssitant");
const { Readable } = require('stream');
const fs = require('fs');
const { default: axios } = require("axios");
const Product = require("../Class/Product");
const bufferToStream = (buffer) => {
    return Readable.from(buffer);
};


   /**
 * openAiTest function sends a chat completion request to the OpenAI API.
 */
const openAiTest = async (req, res)=>{
    try {
        // Send a chat completion request with a user message 'Say this is a test'
        const chatCompletion = await global.openai.chat.completions.create({
            messages: [{ role: 'user', content: 'Say this is a test' }],
            model: 'gpt-3.5-turbo',
        });

        // If the request is successful, respond with a 200 status code and the chat completion
        return res.status(200).json(chatCompletion);
    } catch (error) {
        // If the request fails, respond with a 500 status code and the error message
        return res.status(400).json({message: error.message});
    }
}

/**
 * createThreads function sends a request to the OpenAI API to create a new thread.
 */
const createThreads = async (req, res)=>{
    try {
        // Send a request to create a new thread
        const emptyThread = await global.openai.beta.threads.create();
        // If the request is successful, respond with a 200 status code and the created thread
        return res.status(200).json(emptyThread);
    } catch (error) {
        // If the request fails, respond with a 500 status code and the error message
        return res.status(400).json({message: error.message});
    }
}

const uploadFile = async (req, res)=>{
    try {
        const { file } = req;
        //const fileStream = Readable.from(file.buffer);
       
        const path = './' + file.originalname;
        fs.writeFileSync(path, file.buffer);
       
        const openai_file = await global.openai.files.create({
            file: fs.createReadStream(path),
            purpose: "assistants",
        });
        fs.unlinkSync(path);
        // If the request is successful, respond with a 200 status code and the created message
        return res.status(200).json(openai_file.id);
    } catch (error) {
        console.log('error', error);
        // If the request fails, respond with a 500 status code and the error message
        return res.status(400).json({message: error.message});
    }

}

/**
 * createMessage function sends a request to the OpenAI API to create a new message in a thread.
 */
const createMessage = async (req, res)=>{
    try {
        // Extract thread_id, content, and role from the request body
        const { thread_id, content, role, file_id } = req.body;

        // Send a request to create a new message in the thread
        const threadMessage = await global.openai.beta.threads.messages.create(
            thread_id,
            { role: role || "user", content: content, attachments: file_id ? [{file_id, tools: [{ type: "code_interpreter" }]}] : null}
        );

        // If the request is successful, respond with a 200 status code and the created message
        return res.status(200).json(threadMessage);
    } catch (error) {
        // If the request fails, respond with a 500 status code and the error message
        return res.status(400).json({message: error.message});
    }
}

const getMesseageFromThread = async (req, res)=>{
    try {
        // Extract thread_id, content, and role from the request body
        const { thread_id } = req.body;

        // Send a request to create a new message in the thread
        const threadMessage = await global.openai.beta.threads.messages.list(
            thread_id
        );

        // If the request is successful, respond with a 200 status code and the created message
        return res.status(200).json(threadMessage);
    } catch (error) {
        // If the request fails, respond with a 500 status code and the error message
        return res.status(400).json({message: error.message});
    }

}

/**
 * This function is used to run a thread and get its messages.
 * It takes a request and a response object as parameters.
 */
const runThreadAndGetMessages = async (req, res)=>{
    try {
        // Extract the thread_id and assistant_id from the request body
        const { thread_id, assistant_id, user_id } = req.body;

        // Create a run for the thread using the assistant_id
        const run = await openai.beta.threads.runs.create(
            thread_id,
            { assistant_id }
        );

        // Define the statuses that indicate a run has finished
        const finishedStatus = ['cancelled', 'failed', 'completed', 'expired'] 

        // Get the initial status of the run
        let statusRun = run.status;
        
        // Keep checking the status of the run until it has finished
        while ( !finishedStatus.includes(statusRun) ) {
            // Retrieve the run to get its current status
            const runResponse = await openai.beta.threads.runs.retrieve(thread_id, run.id);
            if(runResponse.status === 'requires_action'){
                await ActiosAssistent.handleRequiresAction(runResponse, thread_id, user_id);
            }
                

            // Update the status
            statusRun = runResponse.status;
        }

        // If the run failed, throw an error
        if(statusRun === 'failed'){
            console.log('run status failed',run)
            throw new Error('Run failed');
        }
            

        // If the run expired, throw an error
        if(statusRun === 'expired')
            throw new Error('Run expired');

        // If the run was cancelled, throw an error
        if(statusRun === 'cancelled')
            throw new Error('Run cancelled');

        // If the run completed successfully, list the messages in the thread
        const threadMessage = await global.openai.beta.threads.messages.list(
            thread_id
        );
  
        // Return a 200 status code and the list of messages
        return res.status(200).json(threadMessage.data);

    } catch (error) {
        // If an error occurred, return a 500 status code and the error message
        console.error(error);
        return res.status(400).json({message: error.message});
    }
}

const smartFind = async (req, res) => {
    try {
      const { search } = req.body.input;
      const { products } = await Product.findProductByNameOrSku(search);
      if(products && products.length)
        return res.status(200).send({ products });
      const { data } = await axios.post("https://yayego.uncodie.com/flow/smart_product_find", { "user_input": search });
      return res.status(200).send({ products: data });
    } catch (error) {
      console.log("error", error);
      res.status(400).send({ error: error.message || error });
    }
  }
module.exports = { 
    smartFind,
    openAiTest, 
    uploadFile,
    createThreads, 
    createMessage, 
    getMesseageFromThread,
    runThreadAndGetMessages
 }
