const express = require('express');
const app = express();
var bodyParser = require('body-parser');
const { OpenAI } = require('openai');
const cron = require('node-cron');
const { productStepOdooToYayego } = require("sync-yayego")
dotenv = require('dotenv');
dotenv.config();
global.openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY, // This is the default and can be omitted
  });
const { helloWorld, openai } = require("./routers");
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use("/", helloWorld);
app.use("/openai", openai);
const cronExpresion = '0 1 * * *';
cron.schedule(cronExpresion, async () => {
  try {
    const response = await productStepOdooToYayego().run();
    console.log("cron sussced", response)
  } catch (error) {
    console.log('error in cron', error)
  }
  
});
module.exports = app;