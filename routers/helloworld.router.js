var express = require("express");
var router = express.Router();
const {
    helloWorld
  } = require("../controllers/helloWorld.actions.js");

router.get("/hello-world", helloWorld);
module.exports = router;