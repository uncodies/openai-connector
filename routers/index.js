const helloWorld = require('./helloworld.router'),
openai = require('./openai.router');

module.exports = {
    helloWorld,
    openai
};