var express = require("express");
var router = express.Router();
const multer = require('multer');
const {
  openAiTest,
  createThreads,
  createMessage,
  runThreadAndGetMessages,
  uploadFile,
  smartFind
} = require("../controllers/assistent.actions.js");
const upload = multer();
router.get("/test", openAiTest);
router.post("/thread", createThreads);
router.post("/thread/upload-file", upload.single('image'), uploadFile);
router.post("/thread/message", createMessage);
router.post("/thread/run-and-get-messages", runThreadAndGetMessages);
router.post("/smart-find", smartFind);
module.exports = router;