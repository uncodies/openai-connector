dotenv = require('dotenv');
const OrderLine = require("../Class/OrderLine");
const Product = require("../Class/Product");
const ActiosAssistent = require("../Class/ActionsAssitant");

jest.mock('../Class/OrderLine');
jest.mock('../Class/Product');

describe('ActiosAssistent', () => {
  describe('create_order_line', () => {
    it('should return success when order line is created', async () => {
      const mockOrder = [{ _id: 'order1' }];
      const mockProduct = [{ _id: 'product1' }];
      OrderLine.getLastOrderByUser.mockResolvedValue({ order: mockOrder });
      Product.findProductBySku.mockResolvedValue({ products: mockProduct });
      OrderLine.addProductToOrder.mockResolvedValue();

      const result = await ActiosAssistent.create_order_line('user1', { id: 'tool1' }, { sku: 'sku1', amount: 1 });
      expect(result).toEqual({ tool_call_id: 'tool1', output: 'success: true' });
    });
  });

  describe('handleRequiresAction', () => {
    it('should return false when there are no tools that require outputs', async () => {
      const run = {};
      const result = await ActiosAssistent.handleRequiresAction(run, 'thread1', 'user1');
      expect(result).toBe(false);
    });

    it('should return true and submit tool outputs when there are tools that require outputs', async () => {
      const run = {
        required_action: {
          submit_tool_outputs: {
            tool_calls: [
              { type: 'function', function: { name: 'create_order_line', arguments: JSON.stringify({ sku: 'sku1', amount: 1 }) }, id: 'tool1' }
            ]
          }
        }
      };
      const mockOpenAI = {
        beta: {
          threads: {
            runs: {
                submitToolOutputs: jest.fn().mockResolvedValue(true),
            }
          },
        },
      };
      global.openai = mockOpenAI;
      const mockOrder = [{ _id: 'order1' }];
      const mockProduct = [{ _id: 'product1' }];
      OrderLine.getLastOrderByUser.mockResolvedValue({ order: mockOrder });
      Product.findProductBySku.mockResolvedValue({ products: mockProduct });
      OrderLine.addProductToOrder.mockResolvedValue();

      const result = await ActiosAssistent.handleRequiresAction(run, 'thread1', 'user1');
      expect(result).toBe(true);
    });
  });
});