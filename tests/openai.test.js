const app = require("../index");
const httpMocks = require('node-mocks-http');
const { uploadFile } = require('../controllers/assistent.actions'); // adjust the path as needed
const supertest = require("supertest");
const fs = require('fs');
const path = require('path');
dotenv = require('dotenv');
dotenv.config();
let thread_id = '';
describe("Tests openai endpoint", () => {
    test("Test openai test", async () => {
        const res = await supertest(app).get("/openai/test");
        expect((res.status === 200)).toBe(true);
    }, 600000);
    test("Test create thread, create message, run and get messages", async () => {
        const res = await supertest(app).post("/openai/thread").send({});
        thread_id = res.body.id;
        const assistant_id = 'asst_SNZH8K8CL7RhTsbNOq2y9XSE'
        expect((res.status === 200)).toBe(true);
        expect((res.body.id.includes('thread'))).toBe(true);
        console.log('thread_id', thread_id);
        const resMessage = await supertest(app)
          .post('/openai/thread/message')
          .send({ thread_id: thread_id, content: 'agrega 2kg de arrachera de res', role: 'user' });
        expect((resMessage.status === 200)).toBe(true);
        expect((resMessage.body.id.includes('msg'))).toBe(true);
        console.log('sennd message', resMessage.body.id);
        const runMessages = await supertest(app)
          .post('/openai/thread/run-and-get-messages')
          .send({ thread_id: thread_id, assistant_id, user_id: 1 });
        expect((runMessages.status === 200)).toBe(true);
        expect(Array.isArray(runMessages.body)).toBe(true);
        console.log('run and get', runMessages.body.map(item => item.content));
        const reduceMessage = runMessages.body.reduce((acc, item) => {
            let tmp = { ...acc };
            if(!tmp[item.role])
                tmp[item.role] = [];
            tmp[item.role] = tmp[item.role].concat(item.content);
            return tmp;
        },{});
       
        expect((reduceMessage['user'].length > 0 && reduceMessage['assistant'].length > 0)).toBe(true);


    }, 600000);
    // // unit test for uploadFile
    // it('should upload a file and return a 200 status code', async () => {
    //   global.openai = {
    //     files: {
    //       create: jest.fn(),
    //     },
    //   };
    //   const mockFile = { buffer: Buffer.from('mock file') };
    //   const req = httpMocks.createRequest({
    //     file: mockFile 
    //   });
    //   const res = httpMocks.createResponse();
  
    //   global.openai.files.create.mockResolvedValue({ id: 'mockId' });
  
    //   await uploadFile(req, res);
  
    //   expect(res.statusCode).toBe(200);
    //   expect(res._getJSONData()).toEqual({ file_id: 'mockId' });
    // });
    // // unit test for uploadFile
    // it('should return a 400 status code if an error occurs', async () => {
    //   global.openai = {
    //     files: {
    //       create: jest.fn(),
    //     },
    //   };
    //   const mockFile = { buffer: Buffer.from('mock file') };
    //   const req = httpMocks.createRequest({
    //     file: mockFile 
    //   });
    //   const res = httpMocks.createResponse();
  
    //   global.openai.files.create.mockRejectedValue(new Error('mock error'));
  
    //   await uploadFile(req, res);
  
    //   expect(res.statusCode).toBe(400);
    //   expect(res._getJSONData()).toEqual({ message: 'mock error' });
    // });
    // integration test for uploadFile
    // test('should upload a file and return a 200 status code', async () => {
    //   const mockFilePath = path.join(__dirname, 'mockFile.txt');
    //   fs.writeFileSync(mockFilePath, 'mock file content');
    //   const response = await supertest(app)
    //       .post('/openai/thread/upload-file')
    //       .attach('file', mockFilePath);
  
    //   expect(response.statusCode).toBe(200);
    //   expect(response.body).toHaveProperty('file_id');
  
    //   // Clean up the mock file
    //   fs.unlinkSync(mockFilePath);
    // });
    // // integration test for uploadFile
    // test('should return a 400 status code if no file is provided', async () => {
    //     const response = await supertest(app)
    //         .post('/openai/thread/upload-file');

    //     expect(response.statusCode).toBe(400);
    // });
    test('should return 400 when thread creation fails', async () => {
        const mockOpenAI = {
            beta: {
                threads: {
                    create: jest.fn().mockRejectedValue(new Error('Failed to create thread')),
                },
            },
        };
        global.openai = mockOpenAI;

        const res = await supertest(app).post('/openai/thread');
        expect(res.statusCode).toEqual(400);
    });
    
    test('should return 200 when thread is successfully created', async () => {
        const mockOpenAI = {
            beta: {
                threads: {
                    create: jest.fn().mockResolvedValue({}),
                },
            },
        };
        global.openai = mockOpenAI;

        const res = await supertest(app).post('/openai/thread');
        expect(res.statusCode).toEqual(200);
    });

    test('should return 200 when message is successfully created', async () => {
        const mockOpenAI = {
          beta: {
            threads: {
              messages: {
                create: jest.fn().mockResolvedValue({}),
              },
            },
          },
        };
        global.openai = mockOpenAI;
    
        const res = await supertest(app)
          .post('/openai/thread/message')
          .send({ thread_id: 'thread1', content: 'Hello', role: 'user' });
        expect(res.statusCode).toEqual(200);
    });
    
    test('should return 400 when message creation fails', async () => {
        const mockOpenAI = {
          beta: {
            threads: {
              messages: {
                create: jest.fn().mockRejectedValue(new Error('Failed to create message')),
              },
            },
          },
        };
        global.openai = mockOpenAI;
    
        const res = await supertest(app)
          .post('/openai/thread/message')
          .send({ thread_id: 'thread1', content: 'Hello', role: 'user' });
        expect(res.statusCode).toEqual(400);
    });

    test('should return 200 when thread is successfully run and messages are retrieved', async () => {
        const mockOpenAI = {
          beta: {
            threads: {
              runs: {
                create: jest.fn().mockResolvedValue({ status: 'queued', id: 'run1' }),
                retrieve: jest.fn().mockResolvedValue({ status: 'completed' }),
              },
              messages: {
                list: jest.fn().mockResolvedValue({ data: [] }),
              },
            },
          },
        };
        global.openai = mockOpenAI;
    
        const res = await supertest(app)
          .post('/openai/thread/run-and-get-messages')
          .send({ thread_id: 'thread1', assistant_id: 'assistant1' });
        expect(res.statusCode).toEqual(200);
    });
    
    test('should return 400 when thread run fails', async () => {
        const mockOpenAI = {
            beta: {
            threads: {
                runs: {
                create: jest.fn().mockResolvedValue({ status: 'queued', id: 'run1' }),
                retrieve: jest.fn().mockResolvedValue({ status: 'failed' }),
                },
            },
            },
        };
        global.openai = mockOpenAI;

        const res = await supertest(app)
            .post('/openai/thread/run-and-get-messages')
            .send({ thread_id: 'thread1', assistant_id: 'assistant1' });
        expect(res.statusCode).toEqual(400);
    });

    test('should return 400 when thread run cancelled', async () => {
        const mockOpenAI = {
            beta: {
            threads: {
                runs: {
                create: jest.fn().mockResolvedValue({ status: 'queued', id: 'run1' }),
                retrieve: jest.fn().mockResolvedValue({ status: 'cancelled' }),
                },
            },
            },
        };
        global.openai = mockOpenAI;

        const res = await supertest(app)
            .post('/openai/thread/run-and-get-messages')
            .send({ thread_id: 'thread1', assistant_id: 'assistant1' });
        expect(res.statusCode).toEqual(400);
    });
    
    test('should return 400 when thread run expired', async () => {
        const mockOpenAI = {
            beta: {
            threads: {
                runs: {
                create: jest.fn().mockResolvedValue({ status: 'queued', id: 'run1' }),
                retrieve: jest.fn().mockResolvedValue({ status: 'expired' }),
                },
            },
            },
        };
        global.openai = mockOpenAI;

        const res = await supertest(app)
            .post('/openai/thread/run-and-get-messages')
            .send({ thread_id: 'thread1', assistant_id: 'assistant1' });
        expect(res.statusCode).toEqual(400);
    });
})