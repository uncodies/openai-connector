const OrderLine = require('../Class/OrderLine'); // adjust the path as needed
dotenv = require('dotenv');
dotenv.config();
describe('OrderLine', () => {
    it('getLastOrderByUser should return the last order by user', async () => {
        const userId = 1;
        try {
            const response = await OrderLine.getLastOrderByUser(userId);
            expect(response.status).toHaveProperty(200);
        }catch(e){
            console.log(e);
        }
        
    });

    it('addProductToOrder should add a product to an order', async () => {
        const orderId = 1;
        const amount = 2;
        const productId = 3;
        const response = await OrderLine.addProductToOrder(orderId, amount, productId);
        expect(response.status).toHaveProperty(200);
    });

    it('updateProductToOrder should update a product in an order', async () => {
        const orderId = 1;
        const amount = 2;
        const productId = 3;
        const response = await OrderLine.updateProductToOrder(orderId, amount, productId);
        expect(response.status).toHaveProperty(200);
    });
});